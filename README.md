
### What is this repository for? ###

This repository is for IaC for [Telerik DevOps Upskill](https://bitbucket.org/saSlim/telerik_devops_upskill/src/master/) repo
it holds Terraform code for provisioning Kubernetes cluster on AKS

### How do I get set up? ###

This guide assumes that you have an account on Microsoft Azure.
If you don't, you can [sign up here](https://azure.microsoft.com/en-us/free/).

#### Setting up the Azure account ####

You can find the official documentation on installing the [Azure CLI here](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli).

* After you install the Azure CLI, you should run: `az version`
* Next, you need to link your account to the Azure CLI: `az login`
* You can now try listing all your AKS clusters with: `az aks list`
* You will first need to get your subscription ID: `az account list`

#### Setting up the Terraform ####

* Before you can create a cluster with Terraform, you should install the binary.
    You can find the instructions on how to install the Terraform CLI from the [official documentation](https://learn.hashicorp.com/tutorials/terraform/install-cli).

* Terraform needs a Service Principal to create resources on your behalf.
    You can create the Service Principal with: `az ad sp create-for-rbac \
  --role="Contributor" \
  --scopes="/subscriptions/SUBSCRIPTION_ID"`

    You should get a JSON payload like this:

    ```
    {
        "appId": "00000000-0000-0000-0000-000000000000",
        "displayName": "azure-cli-2021-02-13-20-01-37",
        "name": "http://azure-cli-2021-02-13-20-01-37",
        "password": "0000-0000-0000-0000-000000000000",
        "tenant": "00000000-0000-0000-0000-000000000000"
    }

    ```

* Make a note of the `appId`, `password`, and `tenant`. You need those to set up Terraform.
* Export the following environment variables:\

    ```

    export ARM_CLIENT_ID=<insert the appId from above>
    export ARM_SUBSCRIPTION_ID=<insert your subscription id>
    export ARM_TENANT_ID=<insert the tenant from above>
    export ARM_CLIENT_SECRET=<insert the password from above>

    ```


* Then you can run:

    ```
    
    terraform init

    terraform plan
    
    ```

    and if happy with the output:

    ```

    terraform apply

    ```

#### TODO ####

* Set up Azure storage to store Terraform state
* Make it work via pipeline with the following steps:
    - Check for hardcoded credentials
    - Check terraform code format
    - Check for infrastructure vulnerabilities with checkov
    - Send terraform plan to chat 
    - Approve with ChatBot 
    - Deploy the infrastructure


